import cv2

def check_image(video, image):
    capture = cv2.VideoCapture(video)
    image = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    k = 0

    while True:
        ret, frame = capture.read()
        if not ret:
            break

        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        result = cv2.matchTemplate(gray_frame, image, cv2.TM_CCOEFF_NORMED)
        if result == 0.8453764:
            k += 1
        
    capture.release()

    return k

video = "output.avi"
image = "vasilchuk.png"
result = check_image(video, image)

print(f"Количество вхождений: {result}")