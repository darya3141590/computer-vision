import numpy as np

files = ['figure1.txt', 'figure2.txt', 'figure4.txt', 'figure5.txt', 'figure6.txt']

for image in files:
    with open(image, 'r') as file:
        max_size = float(file.readline().strip())
        data = np.loadtxt(file, dtype=int)
    coordinates_of_ones = np.nonzero(data)
    pixels = len(np.unique(coordinates_of_ones[0]))

    nominal_resolution = max_size / pixels if pixels != 0 else print("Изображение пустое")
    try:
        print(f'Изображение {image}: Номинальное разрешение: {nominal_resolution:.2f} мм/пиксель')
    except TypeError:
        pass

