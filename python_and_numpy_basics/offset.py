import numpy as np

def load_image(filename):
    with open(filename, 'r') as file:
        max_size = float(file.readline().strip())
        data = np.loadtxt(file, dtype=int)
    return data


def find_coordinates(image):
    coordinates_of_ones = np.nonzero(image)
    y0, x0 = np.min(coordinates_of_ones, axis=1)
    y1, x1 = np.max(coordinates_of_ones, axis=1)
    return y0, x0, y1, x1


img1 = load_image('img1.txt')
img2 = load_image('img2.txt')

img1_y0, img1_x0, img1_y1, img1_x1 = find_coordinates(img1)
img2_y0, img2_x0, img2_y1, img2_x1 = find_coordinates(img2)

offset_y = img1_y0 - img2_y0
offset_x = img1_x0 - img2_x0
print(f"Смещение по y: {offset_y}")
print(f"Смещение по x: {offset_x}")
