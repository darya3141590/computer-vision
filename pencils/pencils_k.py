import os
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu
from skimage.measure import label, regionprops

folder_path = 'images'
image_files = [f for f in os.listdir(folder_path) if f.endswith('.jpg')]

pencils_sum = 0
for image_file in image_files:
    image_path = os.path.join(folder_path, image_file)
    image = plt.imread(image_path)
    image = image.mean(2)
    thresh = threshold_otsu(image)
    binary = image < thresh * 1.12

    labeled_image = label(binary)
    props = regionprops(labeled_image)

    sorted_regions = len(list(region for region in props if (region.major_axis_length > 2900 and region.major_axis_length < 3200)))
    pencils_sum += sorted_regions

    print(f"Количество карандашей на данном фото: {sorted_regions}")
    plt.imshow(image)
    plt.show()

print(f"Количество карандашей в общем счете: {pencils_sum}")