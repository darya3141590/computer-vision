from skimage.measure import label
import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage import binary_hit_or_miss


def remove_leading_zeros(row):
    return row[First_non_zero_index:Last_non_zero_index+1]
image = np.load(r"ps.npy.txt")
labeled_image = label(image)
unique_objects = len(np.unique(labeled_image)) - 1
mask_dict = {}
all_types_masks = []
for c in range(1, np.max(labeled_image) + 1):
    new_image = np.zeros_like(image)
    new_image[labeled_image == c] = 1
    ones_indices = [i for i, row in enumerate(new_image) if 1 in row]
    group = []
    for i, row in enumerate(new_image):
        if i in ones_indices:
            group.append(row)
    group = np.array(group)
    coordinates_of_ones = np.nonzero(group)
    First_non_zero_index = np.unique(coordinates_of_ones[1])[0]
    Last_non_zero_index = np.unique(coordinates_of_ones[1])[-1]
    new_group = np.apply_along_axis(remove_leading_zeros, axis=1, arr=group)
    if str(new_group) in mask_dict:
        continue
    else:
        mask_dict[str(new_group)] = np.sum(binary_hit_or_miss(image, new_group))
        print(f"Маска: {new_group}")
        print(f"Количество: {mask_dict[str(new_group)]}")
        plt.imshow(new_group)
        plt.show()
print(f"Всего объектов: {unique_objects}")