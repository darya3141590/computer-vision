import os
import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label, regionprops

folder_path = 'out'
image_files = [f for f in os.listdir(folder_path) if f.endswith('.npy')]
sorted_image_files = sorted(image_files, key=lambda x: int(''.join(filter(str.isdigit, x))))

obj1_x = []
obj1_y = []
obj2_x = []
obj2_y = []
for image_file in sorted_image_files:
    image_path = os.path.join(folder_path, image_file)
    image = np.load(image_path)
    labeled_image = label(image)
    props = regionprops(labeled_image)
    sorted_props = sorted(props, key=lambda prop: prop.area)
    
    y1, x1 = sorted_props[0].centroid
    y2, x2 = sorted_props[1].centroid

    obj1_x.append(x1)
    obj1_y.append(y1)
    obj2_x.append(x2)
    obj2_y.append(y2)

plt.title("Trajectories")
plt.plot(obj1_x, obj1_y, label='Object 1')
plt.plot(obj2_x, obj2_y, label='Object 2')
plt.legend()
plt.show()
