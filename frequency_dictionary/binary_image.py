import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label, regionprops


def has_vline(arr, col):
    return 1. == arr.mean(0)[col]

def recognize(prop):
    euler_number = prop.euler_number
    if euler_number == -1:
        if has_vline(prop.image, 0):
            return "B"
        else:
            return "8"
    elif euler_number == 0:
        tmp = prop.image.copy()
        tmp[tmp.shape[0] // 2, :] = 1
        tmp_labeled = label(tmp)
        tmp_props = regionprops(tmp_labeled)
        tmp_euler = tmp_props[0].euler_number
        y, x = prop.centroid_local
        y /= prop.image.shape[0]
        x /= prop.image.shape[1]
        if ((np.isclose(x, y, 0.06)) and (has_vline(prop.image, 0) == False) and (tmp_euler == -1)):
            return "0"
        elif ((has_vline(prop.image, 0) == False) and (tmp_euler == -1)):
            return "A"
        elif ((tmp_euler == 0) and (has_vline(prop.image, 0))):
            return "P"
        elif ((tmp_euler == 0) and (has_vline(prop.image, 0) == False)):
            return "*"
        else:
            return "D"
        
    else:
        if prop.image.mean() == 1.0:
            return "-"
        else:
            if has_vline(prop.image, len(prop.image.mean(0))//2):
                return "1"
            else:
                tmp = prop.image.copy()
                tmp[[0, -1], :] = 1
                tmp[:, [0, -1]] = 1
                tmp_labeled = label(tmp)
                tmp_props = regionprops(tmp_labeled)
                tmp_euler = tmp_props[0].euler_number
                if tmp_euler == -3:
                    return "X"
                elif tmp_euler == -1:
                    return "/"
                else:
                    if prop.eccentricity > 0.5:
                        return "W"
                    else:
                        return "*"

    return "_"

image = plt.imread(r"symbols.png")

image = image.mean(2)
image= image > 0

labeled_image = label(image)
props = regionprops(labeled_image)
result = {}
for prop in props:
    symbol = recognize(prop)
    if symbol not in result:
        result[symbol] = 0
    result[symbol] += 1

print(result)
print(f"Общее количество символов: {np.max(labeled_image)}")
for symbol, k in result.items():
    print(f"{symbol}: {((k/np.max(labeled_image))*100):.1f}%")

plt.imshow(image)
plt.show()