import matplotlib.pyplot as plt 
import numpy as np 
from skimage.measure import regionprops 
from skimage.morphology import label 
from skimage.color import rgb2hsv 


image = plt.imread("balls_and_rects.png") 
labeled = label(image.mean(2) > 0) 

hsv = rgb2hsv(image) 
h = hsv[:, :, 0] 
print(f"Всего фигур: {np.max(labeled)}")

rects_colors = [] 
circles_colors = []

for region in regionprops(labeled): 
    bbox = region.bbox 
    r = h[bbox[0]:bbox[2], bbox[1]:bbox[3]] 
    if len(np.unique(r)[1:]) == 0:
        rects_colors.extend([np.unique(r)[0]])
    else:
        circles_colors.extend([np.unique(r)[1]])
    
    
def color_groups(colors):
    result = []
    while colors:
        color1 = colors.pop(0)
        result.append([color1])

        for color2 in colors.copy():
            if abs(color1 - color2) < 0.1:
                result[-1].append(color2) 
                colors.pop(colors.index(color2)) 
    return result

print("Количество прямоугольников по оттенкам: ")
for each in color_groups(rects_colors):
    print(len(each))
print("Количество кругов по оттенкам: ")
for each in color_groups(circles_colors):
    print(len(each))


plt.imshow(image) 
plt.show() 

